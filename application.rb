# 1. Найдите в документации Ruby по адресу методы для объектов разных классов.

# Fixnum:
# Метод, возвращающий вещественный результат от деления
5.fdiv(3)
# Метод проверяющий, является ли число нечётным
3.odd?

# Integer:
# Метод, возвращающий Наибольший Общий Делитель 2-х чисел
57.gcd(27)
# Метод, позволяющий итерировать от одного числа до другого
# up
65.upto(40) { |i| print i, " " }
# down
5.downto(1) { |n| print n, " " }
# Метод приведения целого числа к рациональному
(25.5).to_r

# Numeric:
# Метод, позволяющий итерировать от данного целого числа с
# указанием шага итерации и числа верхнего предела итерации
10.step(30, 2) { |i| print i, " " }
30.step(10, -2) { |i| print i, " " }
# Метод, приводящий данное число к комплексному (мнимому)
22.to_c

# Float:
# Метод, приводящий вещественное число к строке
87.22.to_s

# Array:
# Метод, возвращающий последний элемент из массива (с его извлечением из массива)
[1, 4, 65].pop
# Метод, добавляющий элемент в конец массива
[55, 57, 43].push(87)
[55, 57, 43] << 87

# Hash:
# Метод, возвращающий массив ключей хэша
hash = { one: 24, two: 65, three: 99}
hash.keys
# Метод, возвращающий массив значений хэша
hash.values

# Range:
# Метод, проверяющий, включено ли последнее значение в диапазон
(1...5).exclude_end?

# 2. Примените условное выражение if-else-end

player = { name: 'Johnny', color: :red }
colors = [:blue, :white, :green, :red, :orange]

def answer (selected_color, true_color)
  if selected_color == true_color
    "ты прав!"
  elsif selected_color.length == true_color.length
    "букв столько же, но значение иное!"
  else
    "попробуй еще раз!"
  end
end

puts "#{player[:name]}, " + answer(colors.sample, player[:color])

# 3. Выводите только те имена, которые соответствуют условию.

arr = ["Emma", "noah", "Olivia", "liam", "Sophia", "mason", "Isabella", "jacob"]

arr.each do |name|
  puts name if name.length < 10 && name == name.capitalize
end

# 4. Посчитайте количество символов в каждом элементе массива:

tools = ["Ruby", "Python", "JavaScript", "Java", ".NET", "HTML", "Clojure"]
hash = Hash.new
# Только здесь непонятно зачем тогда inject, а не each
tools.inject(0) do |sum, tool|
  hash[tool] = tool.length
end
puts hash

# 5. Напишите метод вычисления последовательности Фибоначчи

def fibonacci(number)
  sequence = Array.new
  sequence[0] = 0
  sequence[1] = 1
  (2..number).each do |n|
    sequence[n] = sequence[n-1] + sequence[n-2]
  end
  sequence
end

puts fibonacci 24
puts fibonacci 32

# 6. Найдите количество одинаковых слов в массиве:
words = ["Triton", "Atlnatia", "Acropolis", "Pegasus", "Galactica", "Astral Queen",
  "Triton", "Galactica", "Cloud 9", "Gemini", "Gemini", "Hitei Kan"]
# Вариант 1
number = words.length - words.uniq.length
# Вариант 2
result = words.inject(Hash.new(0)) do |hash, i|
  hash[i] += 1
  hash
end
puts result
